// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
/*
 * Copyright (c) 2020 Rockchip Electronics Co., Ltd.
 *
 */

//rk3566-evb1-ddr4-v10
#include "rk3566-evb1-ddr4-v10.dtsi"
#include "rk3568-linux.dtsi"
#include "rp-mipi-camera-gc2093-rk3566.dtsi"

#include "lcd-gpio-rp-box-rk3566.dtsi"	    // lcd pins configuration

/***************** SINGLE LCD (LCD + HDMI) ****************/
/* HDMI only */
//#include "rp-lcd-hdmi.dtsi"

/* MIPI DSI0 */
//#include "rp-lcd-mipi0-5-720-1280-v2-boxTP.dtsi"
#include "rp-lcd-mipi0-7-720-1280.dtsi"
//#include "rp-lcd-mipi0-8-800-1280-v3.dtsi"
//#include "rp-lcd-mipi0-8-1200-1920-v2.dtsi"
//#include "rp-lcd-mipi0-10-800-1280-v3.dtsi"
//#include "rp-lcd-mipi0-10-1200-1920.dtsi"

/** LVDS */
//#include "rp-lcd-lvds-7-1024-600.dtsi"
//#include "rp-lcd-lvds-10-1024-600-raw.dtsi"
//#include "rp-lcd-lvds-10-1280-800.dtsi"

/* EDP */
//#include "rp-lcd-edp-13-1920-1080.dtsi"
//#include "rp-lcd-edp-13.3-15.6-1920-1080.dtsi"





/ {
	model = "rpdzkj rp-box-rk3566 base on Rockchip RK3566 EVB1 DDR4 V10 Board";
	compatible = "rpdzkj,rp-box-rk3566", "rockchip,rk3566";


    rp_power {
        compatible = "rp_power";
        rp_not_deep_sleep = <1>;
        status = "okay";

        led {       //system led
                gpio_num = <&gpio3 RK_PC2 GPIO_ACTIVE_HIGH>;
                gpio_function = <4>;
        };
    };


    rp_gpio{
                status = "okay";
                compatible = "rp_gpio";
                
                /**
                 * gpioxxx {                    // the node name will display on /proc/rp_gpio/, you can define any character string
                 *      gpio_num = <>;          // gpio you want ot control
                 *      gpio_function = <>;     // function of current gpio: 0 output, 1 input, 3 blink
                 *      gpio_event = <KEY_F14>; // optional property used to define gpio report event such as KEY_F14, only works in case of gpio_function = <1>;
                 *    };
                 */
                 
                /******* sytem power en pin, donnot change it only if you know what you are doing */
                vdd_en {	//vdd 3.3v 5v enable
                        gpio_num = <&gpio2 RK_PB3 GPIO_ACTIVE_HIGH>;
                        gpio_function = <0>;
                };
                
                vdd3g_en {	    //vdd_3G 3.3v enable
                        gpio_num = <&gpio0 RK_PC7 GPIO_ACTIVE_HIGH>;
                        gpio_function = <0>;
                };
                
                hub_rst {       //usb hub reset
                        gpio_num = <&gpio0 RK_PB0 GPIO_ACTIVE_HIGH>;
                        gpio_function = <0>;
                };

                spk_en {       //SPK ENABLE
                        gpio_num = <&gpio4 RK_PC4 GPIO_ACTIVE_HIGH>;
                        gpio_function = <0>;
                };

                spk_mute {       //SPK MUTE, hish active, nomal low
                        gpio_num = <&gpio0 RK_PA0 GPIO_ACTIVE_LOW>;
                        gpio_function = <0>;
                };
                otg_idctl {       //OTG SWITCH, high is mean otg_id to 0, foece host mode
                        gpio_num = <&gpio0 RK_PC2 GPIO_ACTIVE_LOW>;
                        gpio_function = <0>;
                };
                
                fan_en {        //fan en
                        gpio_num = <&gpio0 RK_PC5 GPIO_ACTIVE_HIGH>;
                        gpio_function = <0>;
                };


                /***** gpio, add you want to control as blow */

                gpio4a0 {
                        gpio_num = <&gpio4 RK_PA0 GPIO_ACTIVE_HIGH>;
                        gpio_function = <0>;
                };

                gpio4a1 {
                        gpio_num = <&gpio4 RK_PA1 GPIO_ACTIVE_HIGH>;
                        gpio_function = <0>;
                };
                gpio4a2 {
                        gpio_num = <&gpio4 RK_PA2 GPIO_ACTIVE_HIGH>;
                        gpio_function = <0>;
                };
                gpio4a3 {
                        gpio_num = <&gpio4 RK_PA3 GPIO_ACTIVE_HIGH>;
                        gpio_function = <0>;
                };               
                gpio1b2 {
                        gpio_num = <&gpio1 RK_PB2 GPIO_ACTIVE_HIGH>;
                        gpio_function = <0>;
                };
                gpio3c1 {
                        gpio_num = <&gpio3 RK_PC1 GPIO_ACTIVE_HIGH>;
                        gpio_function = <0>;
                };
                gpio1b0 {
                        gpio_num = <&gpio1 RK_PB0 GPIO_ACTIVE_HIGH>;
                        gpio_function = <0>;
                };
                gpio0c4 {
                        gpio_num = <&gpio0 RK_PC4 GPIO_ACTIVE_HIGH>;
                        gpio_function = <0>;
                };                
                gpio1a4 {
                        gpio_num = <&gpio1 RK_PA4 GPIO_ACTIVE_HIGH>;
                        gpio_function = <0>;
                };                
                gpio2b4 {
                        gpio_num = <&gpio2 RK_PB4 GPIO_ACTIVE_HIGH>;
                        gpio_function = <0>;
                };              
	};
};


&pmu_io_domains {
        status = "okay";
        pmuio2-supply = <&vcc3v3_pmu>;
        vccio1-supply = <&vccio_acodec>;
        vccio3-supply = <&vccio_sd>;
        vccio4-supply = <&vcc_3v3>;
        vccio5-supply = <&vcc_3v3>;
        vccio6-supply = <&vcc_1v8>;
        vccio7-supply = <&vcc_3v3>;
};

&hdmi_sound{
        status = "disabled";
};
#if 0
&rk809_codec {
	hp-volume = <20>;	/* 3(max)-255 */
	spk-volume = <100>;	/* 3(max)-255 */
	capture-volume = <10>;	/* 0(max)-255 */
};
#endif

&i2c5 {
    status = "disabled";
};

&i2c0 {
        status = "okay";
        vdd_cpu: tcs4525@1c {
                compatible = "tcs,tcs452x";
                reg = <0x1c>;
                vin-supply = <&vcc5v0_sys>;
                regulator-compatible = "fan53555-reg";
                regulator-name = "vdd_cpu";
                regulator-min-microvolt = <712500>;
                regulator-max-microvolt = <1390000>;
                regulator-ramp-delay = <2300>;
                fcs,suspend-voltage-selector = <1>;
                regulator-boot-on;
                regulator-always-on;
                regulator-state-mem {
                        regulator-off-in-suspend;
                };
        };
};

&gmac1 {
    tx_delay = <0x42>;
    rx_delay = <0x2d>;
};

&i2c4 {
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&i2c4m1_xfer>;

        rtc@51 {
                status = "okay";
                compatible = "rtc,hym8563";
                reg = <0x51>;
        };
};

&uart3 {
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&uart3m0_xfer>;
};

&uart6 {
        status = "okay";
        pinctrl-names = "default";
        pinctrl-0 = <&uart6m0_xfer>;
};

&uart7 {
        status = "okay";
        pinctrl-names = "default";
        pinctrl-0 = <&uart7m0_xfer>;
};

&uart9 {
        status = "okay";
        pinctrl-names = "default";
        pinctrl-0 = <&uart9m0_xfer>;
};

&spi1 {
        status = "okay";
        /* rewrite pinctrl, for cs1 used to be gpio */
		pinctrl-0 = <&spi1m0_cs0 &spi1m0_pins>;
		pinctrl-1 = <&spi1m0_cs0 &spi1m0_pins_hs>;
};

&spi2 {
        status = "okay";
        /* rewrite pinctrl, for cs1 used to be gpio */
		pinctrl-0 = <&spi2m0_cs0 &spi2m0_pins>;
		pinctrl-1 = <&spi2m0_cs0 &spi2m0_pins_hs>;
};

&spi3 {
	status = "okay";
};

/****** rp3566 camera configuration adjustment ******/
&spi3 {
    /* rewrite pinctrl for cs1 used to be camera clk */
		pinctrl-0 = <&spi3m0_cs0 &spi3m0_pins>;
		pinctrl-1 = <&spi3m0_cs0 &spi3m0_pins_hs>;
};
/***************************************************/

&dmc {
    status = "disabled";
};
&dfi {
	status = "disabled";
};

&pdm{
	status = "disabled";
};


